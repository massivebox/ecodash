package tools

import (
	"crypto/sha256"
	"fmt"
	"html/template"
	"math"
	"strconv"
	"time"
)

// just a little utility function to SHA256 strings (for hashing passwords).
func Hash(toHash string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(toHash)))
}

func TemplateDivide(num1, num2 float32) template.HTML {
	division := float64(num1 / num2)

	if math.IsNaN(division) || division == 0 {
		return "0"
	}

	powerOfTen := int(math.Floor(math.Log10(division)))
	if powerOfTen >= -2 && powerOfTen <= 2 {
		// #nosec G203 // We're only printing floats
		return template.HTML(strconv.FormatFloat(math.Round(division*100)/100, 'f', -1, 64))
	}

	preComma := division / math.Pow10(powerOfTen)
	// #nosec G203 // We're only printing floats
	return template.HTML(fmt.Sprintf("%s * 10<sup>%d</sup>", strconv.FormatFloat(math.Round(preComma*100)/100, 'f', -1, 64), powerOfTen))
}

func TemplateHTMLDateFormat(date time.Time) template.HTML {
	if date.IsZero() {
		return ""
	}
	// #nosec G203 // We're only printing a date
	return template.HTML(date.Format("2006-01-02"))
}
