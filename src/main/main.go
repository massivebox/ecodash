package main

import (
	"log"
	"os"

	"git.massivebox.net/ecodash/ecodash/src/ecodash"
	"git.massivebox.net/ecodash/ecodash/src/tools"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/template/html"
	"github.com/robfig/cron/v3"
)

func main() {
	config, err := ecodash.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	cr := cron.New()
	_, err = cr.AddFunc("@hourly", config.UpdateHistory)
	if err != nil {
		log.Fatal(err)
	}
	cr.Start()
	config.UpdateHistory()

	engine := html.New("./templates/"+config.Dashboard.Theme, ".html")
	engine.AddFunc("divide", tools.TemplateDivide)
	engine.AddFunc("HTMLDateFormat", tools.TemplateHTMLDateFormat)

	app := fiber.New(fiber.Config{
		Views: engine,
	})

	app.Static("/assets", "./templates/"+config.Dashboard.Theme+"/assets")

	app.Get("/", func(c *fiber.Ctx) error {
		if config.Administrator.Username == "" || config.Administrator.PasswordHash == "" {
			c.Cookie(&fiber.Cookie{Name: "admin_username", Value: ""})
			c.Cookie(&fiber.Cookie{Name: "admin_password_hash", Value: tools.Hash("")})
			return config.RenderAdminPanel(c, nil)
		}
		return config.RenderIndex(c)
	})

	app.Get("/accuracy-notice", func(c *fiber.Ctx) error {
		return c.Render("accuracy-notice", config.TemplateDefaultsMap(), "base")
	})

	app.All("/admin", config.AdminEndpoint)

	port := os.Getenv("PORT")
	if port == "" {
		port = "80"
	}
	log.Fatal(app.Listen(":" + port))
}
