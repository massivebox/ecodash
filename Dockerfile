FROM golangci/golangci-lint:latest-alpine
FROM golang:alpine

COPY --from=0 /usr/bin/golangci-lint /usr/bin/golangci-lint
RUN apk add --no-cache gcc libc-dev

WORKDIR /app
COPY src /app/src
COPY go.mod /app/
COPY .golangci.yml /app/

RUN go mod tidy; \
    golangci-lint run; \
    go test ./src/...
RUN CGO_ENABLED=1 go build -o app src/main/main.go

FROM alpine:latest

WORKDIR /app

COPY --from=1 /app/app .
COPY ./templates /app/templates

RUN mkdir data
ENV DATABASE_PATH=./data/database.db
ENV CONFIG_PATH=./data/config.json

CMD "./app"