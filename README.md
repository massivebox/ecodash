# 🌿 EcoDash

[![Support the project](https://cloud.massivebox.net/index.php/s/DcxB6KwkDZALbXw/download?path=%2FEcoDash&files=support-the-project.svg)](https://massivebox.net/pages/donate.html)

EcoDash is a simple way to show your users how much your server consumes.  
It's intended as a medium of transparency, that gives your users an idea about the consumption of your machine. It's not meant to be 100% accurate.

You can see it in action here: https://ecodash.massivebox.net

## Get started

Check out the documentation in our [wiki](https://git.massivebox.net/massivebox/ecodash/wiki) to get started with EcoDash.

- [📖 Introduction](https://git.massivebox.net/massivebox/ecodash/wiki)
- [⬇️ Installation](https://git.massivebox.net/massivebox/ecodash/wiki/install)
- [⚙️ Configuration](https://git.massivebox.net/massivebox/ecodash/wiki/config)

## License

```
EcoDash
Copyright (C) 2022 MassiveBox

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

